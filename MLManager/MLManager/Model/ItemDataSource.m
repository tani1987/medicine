//
//  ItemDataSource.m
//  MLManager
//
//  Created by 黒田 真次郎 on 2015/06/27.
//  Copyright (c) 2015年 黒田 真次郎. All rights reserved.
//

#import "ItemDataSource.h"

@implementation ItemDataSource

+ (NSArray *)medicalSpecialistArray
{
    NSArray *ary = [NSArray arrayWithObjects:@"内科", @"外科", nil];
    NSArray *ret = [NSArray arrayWithObjects:ary, nil];
    return ret;
}

+ (NSArray *)workplaceArray
{
    NSArray *ary = [NSArray arrayWithObjects:
                    @"北海道", @"青森県", @"岩手県", @"宮城県", @"秋田県",
                    @"山形県", @"福島県", @"茨城県", @"栃木県", @"群馬県",
                    @"埼玉県", @"千葉県", @"東京都", @"神奈川県", @"新潟県",
                    @"富山県", @"石川県", @"福井県", @"山梨県", @"長野県",
                    @"岐阜県", @"静岡県", @"愛知県", @"三重県", @"滋賀県",
                    @"京都府", @"大阪府", @"兵庫県", @"奈良県", @"和歌山県",
                    @"鳥取県", @"島根県", @"岡山県", @"広島県", @"山口県",
                    @"徳島県", @"香川県", @"愛媛県", @"高知県", @"福岡県",
                    @"佐賀県", @"長崎県", @"熊本県", @"大分県", @"宮崎県",
                    @"鹿児島県", @"沖縄県", nil];
    NSArray *ret = [NSArray arrayWithObjects:ary, nil];
    return ret;
}

+ (NSArray *)graduateYearArray
{
    // 一番古い年を定義
    const NSInteger FIRST_YEAR = 1970;
    // 現在の日付を取得
    NSDate *today = [NSDate date];
    // 現在の日付(NSDate)から年と月をintで取得
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear fromDate:today];
    // 現在の年を取得
    int currentYear = components.year;
    NSMutableArray *years = [[NSMutableArray alloc] initWithCapacity:(currentYear - FIRST_YEAR)];
    for (int i = FIRST_YEAR; i <= currentYear; i++) {
        [years addObject:[NSString stringWithFormat:@"%d年度", i]];
    }
    NSArray *ret = [NSArray arrayWithObjects:years, nil];
    return ret;
}

+ (NSArray *)acquisitionYearArray
{
    // 一番古い年を定義
    const NSInteger FIRST_YEAR = 1970;
    // 現在の日付を取得
    NSDate *today = [NSDate date];
    // 現在の日付(NSDate)から年と月をintで取得
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear fromDate:today];
    // 現在の年を取得
    int currentYear = components.year;
    NSMutableArray *years = [[NSMutableArray alloc] initWithCapacity:(currentYear - FIRST_YEAR)];
    for (int i = FIRST_YEAR; i <= currentYear; i++) {
        [years addObject:[NSString stringWithFormat:@"%d年", i]];
    }
    NSArray *ret = [NSArray arrayWithObjects:years, nil];
    return ret;
}

+ (NSArray *)requiredUnitCheckListArray
{
    NSArray *ary = [NSArray arrayWithObjects:@"総会に2回出席", nil];
    NSArray *ret = [NSArray arrayWithObjects:ary, nil];
    return ret;
}

@end

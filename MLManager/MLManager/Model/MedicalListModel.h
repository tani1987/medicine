//
//  MedicalListModel.h
//  LMS
//
//  Created by VV on 2015/01/06.
//  Copyright (c) 2015年 黒田 真次郎. All rights reserved.
//

#import "MTLModel.h"
#import "MedicalModel.h"
#import <MTLJSONAdapter.h>

@interface MedicalListModel : MTLModel<MTLJSONSerializing>

// 専門医の配列(idとname)
@property (nonatomic) NSArray *medicals;

@end

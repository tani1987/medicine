//
//  MedicalListModel.m
//  LMS
//
//  Created by VV on 2015/01/06.
//  Copyright (c) 2015年 黒田 真次郎. All rights reserved.
//

#import "MedicalListModel.h"
#import <NSValueTransformer+MTLPredefinedTransformerAdditions.h>

@implementation MedicalListModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{};
}

+ (NSValueTransformer *)medicalsJSONTransformer
{
    return [NSValueTransformer mtl_JSONArrayTransformerWithModelClass:MedicalModel.class];
}

@end

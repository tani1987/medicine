//
//  MedicalModel.h
//  LMS
//
//  Created by 黒田 真次郎 on 2015/02/11.
//  Copyright (c) 2015年 黒田 真次郎. All rights reserved.
//

#import "MTLModel.h"
#import <MTLJSONAdapter.h>

@interface MedicalModel : MTLModel<MTLJSONSerializing>

@property (nonatomic) NSString *id; // id
@property (nonatomic) NSString *name; // 取得済専門医の名前

@end

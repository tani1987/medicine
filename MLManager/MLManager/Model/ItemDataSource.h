//
//  ItemDataSource.h
//  MLManager
//
//  Created by 黒田 真次郎 on 2015/06/27.
//  Copyright (c) 2015年 黒田 真次郎. All rights reserved.
//

#import <Foundation/Foundation.h>

// ピッカーに表示するアイテムの定義クラス
@interface ItemDataSource : NSObject

// 取得専門医
+ (NSArray *)medicalSpecialistArray;

// 勤務先都道府県
+ (NSArray *)workplaceArray;

// 大学卒業年度
+ (NSArray *)graduateYearArray;

// 取得年
+ (NSArray *)acquisitionYearArray;

// 必須単位取得チェックリスト
+ (NSArray *)requiredUnitCheckListArray;

@end

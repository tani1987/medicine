//
//  RegisterInfoModel.h
//  LMS
//
//  Created by 黒田 真次郎 on 2015/02/01.
//  Copyright (c) 2015年 黒田 真次郎. All rights reserved.
//

#import "MTLModel.h"
#import <MTLJSONAdapter.h>

@interface RegisterInfoModel : MTLModel<MTLJSONSerializing>

@property (nonatomic) NSString *mailAddress;
@property (nonatomic) NSString *password;
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *medicalSpecialist;
@property (nonatomic) NSString *workplace;
@property (nonatomic) NSString *classOfYear;
@property (nonatomic) NSString *acquisitionYear;
@property (nonatomic) NSString *updateLimit;
@property (nonatomic) NSString *numOfUnit;
@property (nonatomic) NSString *requiredUnitCheckList;

@end

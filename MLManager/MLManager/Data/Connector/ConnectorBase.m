//
//  ConnectorBase.m
//  LMS
//
//  Created by 黒田 真次郎 on 2014/12/21.
//  Copyright (c) 2014年 黒田 真次郎. All rights reserved.
//

#import "ConnectorBase.h"

@implementation ConnectorBase

- (BOOL)postForJSON:(NSString *)url param:(NSDictionary *)param completionHandler:(void (^)(NSDictionary *data))completionHandler errorHandler:(void (^)(NSError *error))errorHandler
{
    //NSLog(@"%@", [param description]);
    
    NSError *error = nil;
    
//    NSMutableDictionary *requestParam = [NSMutableDictionary dictionary];
//    [requestParam setValue:param forKey:SL_JSON_KEY_COMMON_DATA];
    
    NSData *dataJSON = [NSJSONSerialization dataWithJSONObject:param options:NSJSONWritingPrettyPrinted error:&error];
    
    if (error) {
        return NO;
    }
    
    NSString *stringJSON = [[NSString alloc] initWithData:dataJSON encoding:NSUTF8StringEncoding];
    
//    NSLog(@"HTTPBody: %@", stringJSON);
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    config.timeoutIntervalForRequest = 30;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate:nil delegateQueue:nil];
    
    NSURL *URL = [NSURL URLWithString:url];
    
    NSMutableURLRequest *mutableURLRequest = [[NSMutableURLRequest alloc] initWithURL:URL];
    
    mutableURLRequest.HTTPMethod = @"POST";
    [mutableURLRequest addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [mutableURLRequest setHTTPBody:[stringJSON dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:mutableURLRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        
//        NSString *str= [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//        NSLog(@"string from data is %@", str);
//        
//        NSLog(@"response: %@", response);
//        NSLog(@"data: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
        
        if (error) {
            //エラー処理呼び出し
            errorHandler(error);
            return;
        }
        
        // 受け取ったレスポンスから、Cookieを取得します。
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        //                        NSLog(@"statusCodeは%d", (int)httpResponse.statusCode);
        NSArray *cookies = [NSHTTPCookie cookiesWithResponseHeaderFields:httpResponse.allHeaderFields forURL:response.URL];
        
        // 受け取ったCookieを保存
        for (int i = 0; i < cookies.count; i++) {
            NSHTTPCookie *cookie = [cookies objectAtIndex:i];
//            NSLog(@"cookie: name=%@, value=%@", cookie.name, cookie.value);
            [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
        }
        
        NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                       options:NSJSONReadingAllowFragments
                                                                         error:nil];
        
//        if ([self isApiStatusError:jsonDictionary]) {
//            errorHandler([self createError:jsonDictionary]);
//        } else {
        completionHandler(jsonDictionary);
//        }
        
        [session invalidateAndCancel];
    }];
    
    [task resume];
    
    return YES;
}

@end

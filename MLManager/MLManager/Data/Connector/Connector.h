//
//  Connector.h
//  LMS
//
//  Created by 黒田 真次郎 on 2014/12/21.
//  Copyright (c) 2014年 黒田 真次郎. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "ConnectorBase.h"

@interface Connector : ConnectorBase

- (BOOL)login:(NSString *)mail password:(NSString *)password completionHandler:(void (^)(NSDictionary *data))completionHandler errorHandler:(void (^)(NSError *error))errorHandler;

- (BOOL)getMedical:(void (^)(NSArray *medicalList))completionHandler errorHandler:(void (^)(NSError *error))errorHandler;

@end

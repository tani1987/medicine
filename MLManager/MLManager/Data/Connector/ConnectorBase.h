//
//  ConnectorBase.h
//  LMS
//
//  Created by 黒田 真次郎 on 2014/12/21.
//  Copyright (c) 2014年 黒田 真次郎. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "PSURLUtil.h"

@interface ConnectorBase : NSObject

- (BOOL)postForJSON:(NSString *)url param:(NSDictionary *)param completionHandler:(void (^)(NSDictionary *data))completionHandler errorHandler:(void (^)(NSError *error))errorHandler;

@end

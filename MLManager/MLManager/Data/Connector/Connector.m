//
//  Connector.m
//  LMS
//
//  Created by 黒田 真次郎 on 2014/12/21.
//  Copyright (c) 2014年 黒田 真次郎. All rights reserved.
//

#import "Connector.h"
#import "RegisterInfoModel.h"
#import "MedicalListModel.h"

NSString * const PRAM_KEY_LOGINID = @"loginId";
NSString * const PRAM_KEY_LOGINPASSWORD = @"password";

// TODO:配列のキーを決めてもらう
NSString * const DIC_KEY_MEDICAL_LIST = @"medicalList";

@implementation Connector

- (BOOL)login:(NSString *)mail password:(NSString *)password completionHandler:(void (^)(NSDictionary *data))completionHandler errorHandler:(void (^)(NSError *error))errorHandler
{
    //リクエストパラメータ
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:mail forKey:PRAM_KEY_LOGINID];
    [param setValue:password forKey:PRAM_KEY_LOGINPASSWORD];
    
    NSString *URLString = [PSURLUtil getFullPath:MEDICAL_TYPE_LIST];
    [self postForJSON:URLString param:param completionHandler:^(NSDictionary *data) {
        completionHandler(data);
    } errorHandler:^(NSError *error) {
        errorHandler(error);
    }];
    
    return YES;
}

- (BOOL)getMedical:(void (^)(NSArray *medicalList))completionHandler errorHandler:(void (^)(NSError *error))errorHandler
{
    //リクエストパラメータ
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    
    NSString *URLString = [PSURLUtil getFullPath:MEDICAL_TYPE_LIST];
    [self postForJSON:URLString param:param completionHandler:^(NSDictionary *data) {
        NSError *error = nil;
        NSArray *medicalList = [MTLJSONAdapter modelsOfClass:[MedicalListModel class] fromJSONArray:[data objectForKey:DIC_KEY_MEDICAL_LIST] error:&error];
        
        completionHandler(medicalList);
    } errorHandler:^(NSError *error) {
        errorHandler(error);
    }];
    
    return YES;
}

@end

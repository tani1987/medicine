//
//  DataController.m
//  LMS
//
//  Created by 黒田 真次郎 on 2014/12/21.
//  Copyright (c) 2014年 黒田 真次郎. All rights reserved.
//

#import "DataController.h"
#import "Connector.h"

@implementation DataController

+ (DataController *)sharedInstance
{    
    static DataController* sharedInstance;
    static dispatch_once_t once;
    dispatch_once( &once, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

- (BOOL) login:(NSString *)mail password:(NSString *)password  completionHandler:(void (^)(NSDictionary *data))completionHandler errorHandler:(void (^)(NSError *error))errorHandler
{
//    NSLog(@"****%s****", __func__);
    
    Connector *authenticateConnector = [[Connector alloc] init];
    return [authenticateConnector login:mail password:password completionHandler:^(NSDictionary *data) {
        completionHandler(data);
    } errorHandler:^(NSError *error) {
        errorHandler(error);
    }];
}

- (BOOL)getMedical:(void (^)(NSArray *medicalList))completionHandler errorHandler:(void (^)(NSError *error))errorHandler
{
    NSLog(@"****%s****", __func__);
    
    Connector *connector = [[Connector alloc] init];
    return  [connector getMedical:^(NSArray *medicalList) {
        completionHandler(medicalList);
    } errorHandler:^(NSError *error) {
        errorHandler(error);
    }];
}

@end

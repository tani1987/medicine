//
//  DataController.h
//  LMS
//
//  Created by 黒田 真次郎 on 2014/12/21.
//  Copyright (c) 2014年 黒田 真次郎. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface DataController : NSObject

+ (DataController *)sharedInstance;

/**
 *  ログインします
 *
 *  @param mail メールアドレス
 *  @param password パスワード
 *  @param completionHandler 完了ハンドラ
 *          status APIステータスコード
 *  @param errorHandler エラーハンドラ
 *
 *  @return 結果
 */
- (BOOL) login:(NSString *)mail password:(NSString *)password completionHandler:(void (^)(NSDictionary *data))completionHandler errorHandler:(void (^)(NSError *error))errorHandler;

/**
 *  取得済専門医情報の取得
 *  @param medicalList　取得済専門医の配列(MedicalModelを持つ)
 *  @return 結果
 */
- (BOOL)getMedical:(void (^)(NSArray *medicalList))completionHandler errorHandler:(void (^)(NSError *error))errorHandler;

@end

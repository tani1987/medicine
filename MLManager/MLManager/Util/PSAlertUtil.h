//
//  PSAlertUtil.h
//  LMS
//
//  Created by VV on 2015/01/04.
//  Copyright (c) 2015年 黒田 真次郎. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface PSAlertUtil : NSObject

/**
 * 警告アラートの表示(タイトル:警告 ボタン:OK)
 *
 * @param viewContoroller 呼び出し元のviewController
 * @param message 文章
 **/
+ (void)showAlertOn:(UIViewController *const)viewController withMessage:(NSString *const)message;

/**
 *  ポップオーバー表示
 *
 *  @param 表示元のUIViewController
 *  @param ポップオーバーさせるUIViewController
 *  @param blurView(表示元のUIViewControllerと同サイズ、ポップアップが二階層の場合はnilが入る)
 *
 */
+ (void)showPopoverView:(UIViewController *)controller popoverViewController:(UIViewController *)popoverViewController blurView:(UIView *)blurView;

/**
 *  ポップオーバー消去
 *
 *  @param ポップオーバーしているUIViewController
 *  @param blurView(表示元のUIViewControllerと同サイズ)
 *
 */
+ (void)dismissPopoverView:(UIViewController *)popoverViewController blurView:(UIView *)blurView;

@end

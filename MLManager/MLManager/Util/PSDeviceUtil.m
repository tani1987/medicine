//
//  PSDeviceUtil.m
//  LMS
//
//  Created by 黒田 真次郎 on 2014/12/03.
//  Copyright (c) 2014年 黒田 真次郎. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "PSDeviceUtil.h"

static NSInteger const iPHONE4_HEIGHT = 480;
static NSInteger const iPHONE5_HEIGHT = 568;
static NSInteger const iPHONE6_HEIGHT = 667;
static NSInteger const iPHONE6_PLUS_HEIGHT = 736;

@implementation PSDeviceUtil

+ (float)iOSVersion
{
    return [[[UIDevice currentDevice] systemVersion] floatValue];
}

+ (CGRect)deviceBounds
{
    return [[UIScreen mainScreen] bounds];
}

+ (deviceType)deviceType
{
    CGFloat height = [self deviceBounds].size.height;
    if (height == iPHONE4_HEIGHT) {
        return iPHONE4;
    } else if (height == iPHONE5_HEIGHT) {
        return iPHONE5;
    } else if (height == iPHONE6_HEIGHT) {
        return iPHONE6;
    } else if (height == iPHONE6_PLUS_HEIGHT) {
        return iPHONE6PLUS;
    }
    return OTHER_DEVICE;
}


@end

//
//  PSAlertUtil.m
//  LMS
//
//  Created by VV on 2015/01/04.
//  Copyright (c) 2015年 黒田 真次郎. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "PSAlertUtil.h"

@implementation PSAlertUtil

+ (void)showAlertOn:(UIViewController *const)viewController withMessage:(NSString *const)message
{
    void (^f)(void);
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        f = ^{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"警告" message:message preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [viewController presentViewController:alert animated:YES completion:nil];
        };
    } else {
        f = ^{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"警告" message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alert show];
        };
    }
    if ([NSThread isMainThread]) {
        f();
    } else {
        dispatch_sync(dispatch_get_main_queue(), f);
    }
}

+ (void)showPopoverView:(UIViewController *)controller popoverViewController:(UIViewController *)popoverViewController blurView:(UIView *)blurView
{
    CGRect popFrame = popoverViewController.view.frame;
    popFrame = controller.view.frame;
    popoverViewController.view.frame = popFrame;
    
    UIWindow* window = [UIApplication sharedApplication].keyWindow;
    window = [[UIApplication sharedApplication].windows objectAtIndex:[[window subviews] count] - 1];
    window.rootViewController = [window rootViewController];
    if (blurView) {
        // OSによって挙動が違うので処理を分ける
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
            [window addSubview:blurView];
        } else {
            [controller.navigationController.view addSubview:blurView];
        }
    }
    
    [window addSubview:popoverViewController.view];
    [controller addChildViewController:popoverViewController];
    
    popoverViewController.view.alpha = 0;
    [UIView animateWithDuration:0.2 animations:^{
        popoverViewController.view.alpha = 1.0;
    } completion:^(BOOL finished) {
        [popoverViewController didMoveToParentViewController:controller];
    }];
}

+ (void)dismissPopoverView:(UIViewController *)popoverViewController blurView:(UIView *)blurView
{
    if (blurView) {
        [blurView removeFromSuperview];
    }
    [popoverViewController willMoveToParentViewController:nil];
    [popoverViewController.view removeFromSuperview];
    [popoverViewController removeFromParentViewController];
}

@end

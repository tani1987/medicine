//
//  PSStringUtil.h
//  LMS
//
//  Created by VV on 2015/01/04.
//  Copyright (c) 2015年 黒田 真次郎. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface PSStringUtil : NSObject

/**
*  文字列が空か調べる
*
*  @param string 調べる文字列
*
*  @return BOOL
*/
+ (BOOL)isBlankString:(NSString *)string;

/**
 *  文字列がメールアドレスの形式かどうか調べる
 *
 *  @param string 調べる文字列
 *
 *  @return BOOL
 */
+ (BOOL)isMailAddressString:(NSString *)string;

/**
 *  最低文字数入力されているか調べる
 *
 *  @param string 調べる文字列
 *  @param min 最低文字数
 *
 *  @return BOOL
 */
+ (BOOL)isOverMinimumChars:(NSString *)string min:(NSInteger)min;

/**
 *  ランダムな文字列を作る
 *
 *  @param length 文字列の長さ
 *
 *  @return ランダムな文字列を返す
 */
+ (NSString *)randStringWithLength:(NSInteger)length;

/**
 *  YYYY-MM-DDをMM/DDに変換
 *
 *  @param  YYYY_MM_DD
 *
 *  @return MM/DDを返す
 */
+ (NSString *)MM_DDFromYYYY_MM_DD:(NSString *)YYYY_MM_DD;

/**
 *  YYYY-MM-DDをNSDateに変換
 *
 *  @param  YYYY_MM_DD
 *
 *  @return NSDate
 */
+ (NSDate *)dateFromString:(NSString *)string;

/**
 *  NSDateをYYYY年MM月DD日に変換
 *
 *  @param  NSDate
 *
 *  @return NSString
 */
+ (NSString *)stringFromDate:(NSDate *)date;
@end

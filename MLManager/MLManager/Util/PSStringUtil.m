//
//  PSStringUtil.m
//  LMS
//
//  Created by VV on 2015/01/04.
//  Copyright (c) 2015年 黒田 真次郎. All rights reserved.
//

#import "PSStringUtil.h"

@implementation PSStringUtil

+ (BOOL)isBlankString:(NSString *)string
{
    if ([string isEqualToString:@""] || !string) {
        return YES;
    } else {
        return NO;
    }
}

+ (BOOL)isMailAddressString:(NSString *)string
{
    NSError *error;
    NSRegularExpression *regax = [NSRegularExpression regularExpressionWithPattern:@"^[\\w\\-.]+@[a-z\\d\\-.]+\\.[a-z]+$"
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    NSTextCheckingResult *result = [regax firstMatchInString:string options:0 range:NSMakeRange(0, string.length)];
    return result ? YES : NO;
}

+ (BOOL)isOverMinimumChars:(NSString *)string min:(NSInteger)min
{
    if (string.length >= min) {
        return YES;
    } else {
        return NO;
    }
}

+ (NSString *)randStringWithLength:(NSInteger)length
{
    unichar letter[length];
    NSInteger max = [@"z" characterAtIndex:0];
    NSInteger min = [@"a" characterAtIndex:0];
    for (int i = 0; i < length; i++) {
        letter[i] = arc4random_uniform(max - min + 1) + min;
    }
    return [[NSString alloc] initWithCharacters:letter length:length];
}

+ (NSString *)MM_DDFromYYYY_MM_DD:(NSString *)YYYY_MM_DD
{
    NSString *MM_DD = [YYYY_MM_DD substringFromIndex:5];
    MM_DD = [MM_DD stringByReplacingOccurrencesOfString:@"-" withString:@"/"];
    return MM_DD;
}

+ (NSDate *)dateFromString:(NSString *)string
{
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy年MM月dd日"];
    //タイムゾーンの指定
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
    return [formatter dateFromString:string];
}

+ (NSString *)stringFromDate:(NSDate *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy年MM月dd日"];
    return [formatter stringFromDate:date];
}

@end

//
//  PSURLUtil.m
//  LMS
//
//  Created by 黒田 真次郎 on 2014/12/21.
//  Copyright (c) 2014年 黒田 真次郎. All rights reserved.
//

#import "PSURLUtil.h"

/** API用URL */
#define URL_BASE @"http://toti.vv-net.co.jp/proseeds/appli/"

@implementation PSURLUtil

+ (NSString *)getPath:(PSApiUrl)url {
    NSString *result = nil;
    
    switch(url) {
        case MEDICAL_TYPE_LIST:
            result = @"getMedical";
            break;
        case UNIT_LIST:
            result = @"getUnit";
            break;
        case REGISTER_USER_INFO:
            result = @"registerUserInfo";
            break;
        case REGISTER_ADDIOTIONAL_UNITS:
            result = @"registerAdditionalUnits";
            break;
        case USER_UNIT:
            result = @"getUnitWithUser";
            break;
        case USER_INFO:
            result = @"getUserInfo";
            break;
    }
    
    return result;
}

+ (NSString *)getFullPath:(PSApiUrl) url
{
    return [NSString stringWithFormat:@"%@%@",URL_BASE,[PSURLUtil getPath:(url)]];
}

@end

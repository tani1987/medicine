//
//  PSDeviceUtil.h
//  LMS
//
//  Created by 黒田 真次郎 on 2014/12/03.
//  Copyright (c) 2014年 黒田 真次郎. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface PSDeviceUtil : NSObject

typedef NS_ENUM(NSInteger, deviceType) {
    /** iPhone4 */
    iPHONE4,
    /** iPhone5 */
    iPHONE5,
    /** iPhone6 */
    iPHONE6,
    /** iPhone6+ */
    iPHONE6PLUS,
    /** Other */
    OTHER_DEVICE,
};

/**
 *  iOSのバージョンを返す
 *
 *  @return 8.0、7.1など
 */
+ (float)iOSVersion;

/**
 *  デバイスのサイズを返す
 *
 *  @return bounds
 */
+ (CGRect)deviceBounds;

/**
 *  デバイスの種類を返す
 *
 *  @return bounds
 */
+ (deviceType)deviceType;

@end

//
//  PSURLUtil.h
//  LMS
//
//  Created by 黒田 真次郎 on 2014/12/21.
//  Copyright (c) 2014年 黒田 真次郎. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface PSURLUtil : NSObject

/** 各パラメータ用列挙体 */
typedef NS_ENUM(NSInteger, PSApiUrl) {
    MEDICAL_TYPE_LIST,
    UNIT_LIST,
    REGISTER_USER_INFO,
    REGISTER_ADDIOTIONAL_UNITS,
    USER_UNIT,
    USER_INFO,
};

/**
 *
 * 対応する文字列を取得する
 **/
+ (NSString *)getFullPath:(PSApiUrl)url;

@end

//
//  UserInfoUtil.m
//  MLManager
//
//  Created by 黒田 真次郎 on 2015/07/14.
//  Copyright (c) 2015年 黒田 真次郎. All rights reserved.
//

#import "UserInfoUtil.h"

@implementation UserInfoUtil

NSString* const FILE_PATH_USER_INFO = @"RegisterInfoModel_UserInfo.dat";

+ (UserInfoUtil *)sharedInstance
{
    static UserInfoUtil* sharedInstance;
    static dispatch_once_t once;
    dispatch_once( &once, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

+ (RegisterInfoModel *)loadUserInfoCache
{
    NSString *directory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    NSString *filePath = [directory stringByAppendingPathComponent:FILE_PATH_USER_INFO];
    
    return (RegisterInfoModel *)[NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
}

+ (BOOL)saveloadUserInfoCache:(RegisterInfoModel *)model
{
    NSString *directory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    NSString *filePath = [directory stringByAppendingPathComponent:FILE_PATH_USER_INFO];
    
    return [NSKeyedArchiver archiveRootObject:model toFile:filePath];
}

@end

//
//  UserInfoUtil.h
//  MLManager
//
//  Created by 黒田 真次郎 on 2015/07/14.
//  Copyright (c) 2015年 黒田 真次郎. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RegisterInfoModel.h"

@interface UserInfoUtil : NSObject

/**
 *  ユーザ情報保存、取得メソッド
 *
 *  @return RegisterInfoModel
 */
+ (RegisterInfoModel *)loadUserInfoCache;
+ (BOOL)saveloadUserInfoCache:(RegisterInfoModel *)models;

@end

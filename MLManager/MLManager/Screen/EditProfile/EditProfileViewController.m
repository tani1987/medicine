//
//  EditProfileViewController.m
//  MLManager
//
//  Created by 黒田 真次郎 on 2015/06/23.
//  Copyright (c) 2015年 黒田 真次郎. All rights reserved.
//

#import "EditProfileViewController.h"
#import "UserInfoUtil.h"

@interface EditProfileViewController ()

@property (nonatomic) RegisterInfoModel *model;

@end

@implementation EditProfileViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.model = [UserInfoUtil loadUserInfoCache];
    
    self.mailAddressTextField.delegate = self;
    self.mailAddressTextField.text = self.model.mailAddress;
    
    self.passwordTextField.delegate = self;
    self.passwordTextField.text = self.model.password;
    
    self.confirmPasswordTextField.delegate = self;
    self.confirmPasswordTextField.text = self.model.password;
    
    self.nameTextField.delegate = self;
    self.nameTextField.text = self.model.name;
    
    self.medicalSpecialistTextField.delegate = self;
    self.medicalSpecialistTextField.text = self.model.medicalSpecialist;
    
    self.workplaceTextField.delegate = self;
    self.workplaceTextField.text = self.model.workplace;
    
    self.classOfYearTextField.delegate = self;
    self.classOfYearTextField.text = self.model.classOfYear;
    
    self.acquisitionYearTextField.delegate = self;
    self.acquisitionYearTextField.text = self.model.acquisitionYear;
    [self setPickerView:self.acquisitionYearTextField items:[ItemDataSource acquisitionYearArray]];
    
    self.updateLimitTextField.delegate = self;
    self.updateLimitTextField.text = self.model.updateLimit;
    [self setDatePickerView:self.updateLimitTextField date:[PSStringUtil dateFromString:self.model.updateLimit]];
    
    self.numOfUnitTextField.delegate = self;
    self.numOfUnitTextField.text = self.model.numOfUnit;
    
    self.requiredUnitCheckListTextField.delegate = self;
    self.requiredUnitCheckListTextField.text = self.model.requiredUnitCheckList;
    [self setPickerView:self.requiredUnitCheckListTextField items:[ItemDataSource requiredUnitCheckListArray] obj:self.requiredUnitCheckListTextField.text];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - ActionMethod

- (IBAction)didTapUpdateButton:(id)sender
{
    // メールアドレスの形式かチェック
    if (![PSStringUtil isMailAddressString:self.mailAddressTextField.text]) {
        [PSAlertUtil showAlertOn:self withMessage:@"メールアドレスの形式で入力をしてください"];
        return;
    }
    // 空の項目がないかチェック
    if (![self validateEmptyTextField:@[self.mailAddressTextField, self.passwordTextField, self.confirmPasswordTextField, self.nameTextField, self.numOfUnitTextField, self.requiredUnitCheckListTextField]]) {
        return;
    }
    // 最低文字数チェック(パスワード)
    if (![PSStringUtil isOverMinimumChars:self.passwordTextField.text min:8]) {
        [PSAlertUtil showAlertOn:self withMessage:@"パスワードは8文字以上で入力してください"];
        return;
    }
    // 最低文字数チェック(パスワード確認用)
    if (![PSStringUtil isOverMinimumChars:self.confirmPasswordTextField.text min:8]) {
        [PSAlertUtil showAlertOn:self withMessage:@"パスワード確認は8文字以上で入力してください"];
        return;
    }
    // パスワード確認が一致しているかどうか
    if (![self.passwordTextField.text isEqualToString:self.confirmPasswordTextField.text]) {
        [PSAlertUtil showAlertOn:self withMessage:@"パスワードの確認が一致しません"];
        return;
    }
    
    self.model.mailAddress = self.mailAddressTextField.text;
    self.model.password = self.passwordTextField.text;
    self.model.name = self.nameTextField.text;
    self.model.medicalSpecialist = self.medicalSpecialistTextField.text;
    self.model.workplace = self.workplaceTextField.text;
    self.model.classOfYear = self.classOfYearTextField.text;
    self.model.acquisitionYear = self.acquisitionYearTextField.text;
    self.model.updateLimit = self.updateLimitTextField.text;
    self.model.numOfUnit = self.numOfUnitTextField.text;
    self.model.requiredUnitCheckList = self.requiredUnitCheckListTextField.text;
    
    [UserInfoUtil saveloadUserInfoCache:self.model];
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end

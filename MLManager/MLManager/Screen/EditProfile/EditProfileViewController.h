//
//  EditProfileViewController.h
//  MLManager
//
//  Created by 黒田 真次郎 on 2015/06/23.
//  Copyright (c) 2015年 黒田 真次郎. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface EditProfileViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UITextField *mailAddressTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextField;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *medicalSpecialistTextField;
@property (weak, nonatomic) IBOutlet UITextField *workplaceTextField;
@property (weak, nonatomic) IBOutlet UITextField *classOfYearTextField;
@property (weak, nonatomic) IBOutlet UITextField *acquisitionYearTextField;
@property (weak, nonatomic) IBOutlet UITextField *updateLimitTextField;
@property (weak, nonatomic) IBOutlet UITextField *numOfUnitTextField;
@property (weak, nonatomic) IBOutlet UITextField *requiredUnitCheckListTextField;

- (IBAction)didTapUpdateButton:(id)sender;

@end

//
//  AddLectureViewController.m
//  MLManager
//
//  Created by 黒田 真次郎 on 2015/06/23.
//  Copyright (c) 2015年 黒田 真次郎. All rights reserved.
//

#import "AddLectureViewController.h"

@interface AddLectureViewController ()<UITextFieldDelegate>

@end

@implementation AddLectureViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden = NO;
    
    // ナビゲーションバーがあり、計算が若干違うのでオーバーライドする
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    
    // テキストフィールドの設定
    self.dateTextField.delegate = self;
    [self setDatePickerView:self.dateTextField];
    
    // 現在の年月日を日時に設定
    NSDate *today = [NSDate date];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:today];
    int currentYear = components.year;
    int currentMonth = components.month;
    int currentDay = components.day;
    self.dateTextField.text = [NSString stringWithFormat:@"%d年%d月%d日", currentYear,currentMonth,currentDay];
    
    self.lectureNameTextField.delegate = self;
    self.detailTextField.delegate = self;
    self.numOfUnitTextField.delegate = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)didTapAddButton:(id)sender
{
    // 空の項目がないかチェック
    if (![self validateEmptyTextField:@[self.lectureNameTextField, self.detailTextField, self.numOfUnitTextField]]) {
        return;
    }
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDelegate Method

- (void)keyboardWillShow:(NSNotification*)note
{
    // キーボードの表示完了時の場所と大きさを取得。
    CGRect keyboardFrameEnd = [[note.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    float screenHeight = screenBounds.size.height;
    
    if((self.navigationController.navigationBar.frame.size.height + self.scrollView.frame.origin.y + self.activeField.frame.origin.y + self.activeField.frame.size.height)>(screenHeight - keyboardFrameEnd.size.height - 20)){
        // テキストフィールドがキーボードで隠れるようなら
        // 選択中のテキストフィールドの直ぐ下にキーボードの上端が付くように、スクロールビューの位置を上げる
        self.scrollView.contentOffset = CGPointMake(0, 0);
        [UIView animateWithDuration:0.3
                         animations:^{
                             self.scrollView.frame = CGRectMake(0, screenHeight - self.activeField.frame.origin.y - self.activeField.frame.size.height - keyboardFrameEnd.size.height - 20, self.scrollView.frame.size.width,self.scrollView.frame.size.height);
                         }];
    }
}

@end

//
//  RegisterInfo3ViewController.m
//  MLManager
//
//  Created by 黒田 真次郎 on 2015/06/23.
//  Copyright (c) 2015年 黒田 真次郎. All rights reserved.
//

#import "RegisterInfo3ViewController.h"
#import "RegisterInfo1ViewController.h"
#import "RegisterInfo2ViewController.h"
#import "LectureListViewController.h"
#import "UserInfoUtil.h"
#import "RegisterInfoModel.h"

@interface RegisterInfo3ViewController ()

@end

@implementation RegisterInfo3ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
        
    // テキストフィールド設定
    // 取得年
    self.acquisitionYearTextField.delegate = self;
    // 現在の年を取得年に設定
    NSDate *today = [NSDate date];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:today];
    int currentYear = components.year;
    self.acquisitionYearTextField.text = [NSString stringWithFormat:@"%d年", currentYear];
    [self setPickerView:self.acquisitionYearTextField items:[ItemDataSource acquisitionYearArray] obj:self.acquisitionYearTextField.text];

    // 更新期限
    self.updateLimitTextField.delegate = self;
    [self setDatePickerView:self.updateLimitTextField];
    int currentMonth = components.month;
    int currentDay = components.day;
    self.updateLimitTextField.text = [NSString stringWithFormat:@"%d年%d月%d日", currentYear,currentMonth,currentDay];
    
    // 取得単位数
    self.numOfUnitTextField.delegate = self;
    
    // 必須単位取得チェックリスト
    self.requiredUnitCheckListTextField.delegate = self;
    [self setPickerView:self.requiredUnitCheckListTextField items:[ItemDataSource requiredUnitCheckListArray] obj:self.requiredUnitCheckListTextField.text];
    
    // 利用規約のテキストフィールド設定
    self.userPolicyTextView.layer.borderWidth = 1.0;
    self.userPolicyTextView.layer.cornerRadius = 4.0;
    self.userPolicyTextView.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Action Method

- (IBAction)didTapBackButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)didTapNextButton:(id)sender
{
    if (!self.isAgreedPolicy) {
        [PSAlertUtil showAlertOn:self withMessage:@"利用規約をご確認ください"];
        return;
    }
    
    RegisterInfo1ViewController *info1 = self.navigationController.viewControllers[0];
    RegisterInfo2ViewController *info2 = self.navigationController.viewControllers[1];
    
    RegisterInfoModel *model = [[RegisterInfoModel alloc] init];
    model.mailAddress = info1.mailAddressTextField.text;
    model.password = info1.passwordTextField.text;
    model.name = info2.nameTextField.text;
    model.medicalSpecialist = info2.medicalSpecialistTextField.text;
    model.workplace = info2.workplaceTextField.text;
    model.classOfYear = info2.classOfYearTextField.text;
    model.acquisitionYear = self.acquisitionYearTextField.text;
    model.updateLimit = self.updateLimitTextField.text;
    model.numOfUnit = self.numOfUnitTextField.text;
    model.requiredUnitCheckList = self.requiredUnitCheckListTextField.text;
    
    [UserInfoUtil saveloadUserInfoCache:model];
    
    LectureListViewController *lectureListVc = [[LectureListViewController alloc] init];
    [self.navigationController pushViewController:lectureListVc animated:YES];
}

- (IBAction)didTapAgreePolicyButton:(id)sender
{
    self.isAgreedPolicy = !self.isAgreedPolicy;
}

@end

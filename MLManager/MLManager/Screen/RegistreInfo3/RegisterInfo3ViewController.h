//
//  RegisterInfo3ViewController.h
//  MLManager
//
//  Created by 黒田 真次郎 on 2015/06/23.
//  Copyright (c) 2015年 黒田 真次郎. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface RegisterInfo3ViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UITextField *acquisitionYearTextField;
@property (weak, nonatomic) IBOutlet UITextField *updateLimitTextField;
@property (weak, nonatomic) IBOutlet UITextField *numOfUnitTextField;
@property (weak, nonatomic) IBOutlet UITextField *requiredUnitCheckListTextField;
@property (weak, nonatomic) IBOutlet UITextView *userPolicyTextView;
@property (weak, nonatomic) IBOutlet UIButton *agreePolicyCheckBox;
@property (nonatomic) BOOL isAgreedPolicy;

- (IBAction)didTapBackButton:(id)sender;
- (IBAction)didTapNextButton:(id)sender;
- (IBAction)didTapAgreePolicyButton:(id)sender;

@end

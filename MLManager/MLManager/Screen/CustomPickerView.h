//
//  CustomPickerView.h
//  MLManager
//
//  Created by 黒田 真次郎 on 2015/06/27.
//  Copyright (c) 2015年 黒田 真次郎. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomPickerView : UIPickerView

@property (nonatomic) NSArray *items;
@property (nonatomic) UITextField *textField;

@end

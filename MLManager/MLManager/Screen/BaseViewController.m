//
//  BaseViewController.m
//  MLManager
//
//  Created by 黒田 真次郎 on 2015/06/25.
//  Copyright (c) 2015年 黒田 真次郎. All rights reserved.
//

#import "BaseViewController.h"
#import "PSStringUtil.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    
    // スクロールビュー設定
    // xibでサイズを大きめに設定しているので画面サイズに戻す
    CGRect frame = self.scrollView.frame;
    frame.size.height = [PSDeviceUtil deviceBounds].size.height;
    self.scrollView.frame = frame;
    self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    CGRect frame = self.scrollView.frame;
    frame.origin.y = 0;
    self.scrollView.frame = frame;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Notification

- (void)keyboardWillShow:(NSNotification*)note
{
    // キーボードの表示完了時の場所と大きさを取得。
    CGRect keyboardFrameEnd = [[note.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    float screenHeight = screenBounds.size.height;
    
    if((self.scrollView.frame.origin.y + self.activeField.frame.origin.y + self.activeField.frame.size.height)>(screenHeight - keyboardFrameEnd.size.height - 20)){
        // テキストフィールドがキーボードで隠れるようなら
        // 選択中のテキストフィールドの直ぐ下にキーボードの上端が付くように、スクロールビューの位置を上げる
        if (self.activeField.frame.origin.y > screenHeight) {
            screenHeight += self.activeField.frame.origin.y - screenHeight + self.activeField.frame.size.height + 20;
        }
        self.scrollView.contentOffset = CGPointMake(0, 0);
        [UIView animateWithDuration:0.3
                         animations:^{
                             self.scrollView.frame = CGRectMake(0, screenHeight - self.activeField.frame.origin.y - self.activeField.frame.size.height - keyboardFrameEnd.size.height - 20, self.scrollView.frame.size.width,self.scrollView.frame.size.height);
                         }];
    }
}

#pragma mark - UITextFieldDelegate Method

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    // メンバ変数activeFieldに選択されたテキストフィールドを代入
    self.activeField = textField;
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [self fixKeyboardPosition];
    // リターンキー押下でキーボード閉じる
    [textField resignFirstResponder];
    return YES;
}

- (void)resignKeyboard:(id)sender
{
    [self fixKeyboardPosition];
    // ピッカーの完了ボタンでキーボード閉じる
    [self.scrollView.subviews enumerateObjectsUsingBlock:^(UIView* obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[UITextField class]]) {
            [obj resignFirstResponder];
        }
    }];
}

#pragma mark - Puclic Method

- (void)setPickerView:(UITextField *)textField items:(NSArray *)items
{
    [self setPickerView:textField items:items obj:nil];
}

- (void)setPickerView:(UITextField *)textField items:(NSArray *)items obj:(id)obj
{
    // ピッカービューの設定
    CustomPickerView *pickerView = [[CustomPickerView alloc] init];
    pickerView.delegate = self;
    pickerView.dataSource = self;
    pickerView.showsSelectionIndicator = YES;
    pickerView.items = items;
    pickerView.textField = textField;
    textField.inputView = pickerView;
    textField.inputAccessoryView = [self makeKeyBoardToolBar];
    
    if (obj) {
        if ([items[0] containsObject:obj]) {
            NSUInteger index = [items[0] indexOfObject:obj];
            [pickerView selectRow:index inComponent:0 animated:NO];
        }
    }
}

- (void)setDatePickerView:(UITextField *)textField
{
    [self setDatePickerView:textField date:[NSDate date]];
}

- (void)setDatePickerView:(UITextField *)textField date:(NSDate *)date
{
    // 日付ピッカーの設定
    UIDatePicker *datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.date = date;
    textField.inputView = datePicker;
    textField.inputAccessoryView = [self makeKeyBoardToolBar];
    
    // 日付ピッカーの値が変更されたときに呼ばれるメソッドを設定
    [datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
}

- (void)fixKeyboardPosition
{
    // viewのy座標を元に戻してキーボードをしまう
    [UIView animateWithDuration:0.2
                     animations:^{
                         self.scrollView.frame = CGRectMake(0, 0, self.scrollView.frame.size.width,self.scrollView.frame.size.height);
                     }];
}

- (UIToolbar *)makeKeyBoardToolBar
{
    // キーボード上のツールバー
    UIToolbar *keyboardToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 38.0f)];
    keyboardToolbar.barStyle = UIBarStyleBlackTranslucent;
    
    // スペース生成
    UIBarButtonItem *spaceBarItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                  target:nil
                                                                                  action:nil];
    // 完了ボタン
    UIBarButtonItem *doneBarItem = [[UIBarButtonItem alloc] initWithTitle:@"完了"
                                                                    style:UIBarButtonItemStyleDone
                                                                   target:self
                                                                   action:@selector(resignKeyboard:)];
    [keyboardToolbar setItems:@[spaceBarItem, doneBarItem]];
    return keyboardToolbar;
}

// 空の項目がないかチェック
- (BOOL)validateEmptyTextField:(NSArray *)textFields
{
    for (UITextField *txtField in textFields) {
        if ([PSStringUtil isBlankString:txtField.text]) {
            [PSAlertUtil showAlertOn:self withMessage:@"未入力の項目があります"];
            return NO;
        }
    }
    return YES;
}

#pragma mark - UIDatePicker Method

- (void)datePickerValueChanged:(id)sender
{
    // ピッカーの日付をテキストフィールドに反映
    UIDatePicker *picker = (UIDatePicker *)sender;
    [self.scrollView.subviews enumerateObjectsUsingBlock:^(UIView* obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[UITextField class]]) {
            if ([obj isFirstResponder]) {
                UITextField *textField = (UITextField *)obj;
                textField.text = [PSStringUtil stringFromDate:picker.date];
            }
        }
    }];
}

#pragma mark - UIPickerViewDelegate Method

/**
 * ピッカーに表示する列数を返す
 */
- (NSInteger)numberOfComponentsInPickerView:(CustomPickerView *)pickerView
{
    return pickerView.items.count;
}

/**
 * ピッカーに表示する行数を返す
 */
- (NSInteger)pickerView:(CustomPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [pickerView.items[component] count];
}

/**
 * 列の幅を変更
 */
- (CGFloat)pickerView:(CustomPickerView *)pickerView widthForComponent:(NSInteger)component
{
    if (!pickerView.items.count) {
        return 0;
    }
    return 320 / pickerView.items.count;
}

/**
 * ピッカーに表示する値を返す
 */
- (NSString *)pickerView:(CustomPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return pickerView.items[component][row];
}

/**
 * ピッカーの選択行が決まったとき
 */
- (void)pickerView:(CustomPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSMutableString *text = [NSMutableString string];
    // 複数列あった場合のために回す i:ピッカーの列 n:アイテムの列
    for (int i = 0; i < pickerView.items.count; i++) {
        NSInteger n = [pickerView selectedRowInComponent:i];
        [text appendString:pickerView.items[i][n]];
    }
    pickerView.textField.text = text;
}

@end

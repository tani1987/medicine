//
//  RegisterInfo1ViewController.m
//  MLManager
//
//  Created by 黒田 真次郎 on 2015/06/23.
//  Copyright (c) 2015年 黒田 真次郎. All rights reserved.
//

#import "RegisterInfo1ViewController.h"

@interface RegisterInfo1ViewController ()

@end

@implementation RegisterInfo1ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // 画面を保持
    self.info2Vc = [[RegisterInfo2ViewController alloc] init];
    self.info3Vc = [[RegisterInfo3ViewController alloc] init];
        
    // テキストフィールド設定
    self.mailAddressTextField.delegate = self;
    self.passwordTextField.delegate = self;
    self.confirmPasswordTextField.delegate = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Action Method

- (IBAction)didTapNextButton:(id)sender
{
    // メールアドレスの形式かチェック
    if (![PSStringUtil isMailAddressString:self.mailAddressTextField.text]) {
        [PSAlertUtil showAlertOn:self withMessage:@"メールアドレスの形式で入力をしてください"];
        return;
    }
    // 空の項目がないかチェック
    if (![self validateEmptyTextField:@[self.mailAddressTextField, self.passwordTextField, self.confirmPasswordTextField]]) {
        return;
    }
    // 最低文字数チェック(パスワード)
    if (![PSStringUtil isOverMinimumChars:self.passwordTextField.text min:8]) {
        [PSAlertUtil showAlertOn:self withMessage:@"パスワードは8文字以上で入力してください"];
        return;
    }
    // 最低文字数チェック(パスワード確認用)
    if (![PSStringUtil isOverMinimumChars:self.confirmPasswordTextField.text min:8]) {
        [PSAlertUtil showAlertOn:self withMessage:@"パスワード確認は8文字以上で入力してください"];
        return;
    }
    // パスワード確認が一致しているかどうか
    if (![self.passwordTextField.text isEqualToString:self.confirmPasswordTextField.text]) {
        [PSAlertUtil showAlertOn:self withMessage:@"パスワードの確認が一致しません"];
        return;
    }
    
    self.info2Vc.info3Vc = self.info3Vc;
    [self.navigationController pushViewController:self.info2Vc animated:YES];
}
@end

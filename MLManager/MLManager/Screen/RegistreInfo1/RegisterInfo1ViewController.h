//
//  RegisterInfo1ViewController.h
//  MLManager
//
//  Created by 黒田 真次郎 on 2015/06/23.
//  Copyright (c) 2015年 黒田 真次郎. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "RegisterInfo2ViewController.h"
#import "RegisterInfo3ViewController.h"

@interface RegisterInfo1ViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UIImageView *topImageView;
@property (weak, nonatomic) IBOutlet UITextField *mailAddressTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextField;

@property (nonatomic) RegisterInfo2ViewController *info2Vc;
@property (nonatomic) RegisterInfo3ViewController *info3Vc;

- (IBAction)didTapNextButton:(id)sender;

@end

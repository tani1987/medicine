//
//  BaseViewController.h
//  MLManager
//
//  Created by 黒田 真次郎 on 2015/06/25.
//  Copyright (c) 2015年 黒田 真次郎. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomPickerView.h"
#import "ItemDataSource.h"
#import "PSAlertUtil.h"
#import "PSStringUtil.h"
#import "PSDeviceUtil.h"

@interface BaseViewController : UIViewController<UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic) UITextField *activeField;

// ピッカービューの設定
- (void)setPickerView:(UITextField *)textField items:(NSArray *)items;
- (void)setPickerView:(UITextField *)textField items:(NSArray *)items obj:(id)obj;
// 日付ピッカーの設定
- (void)setDatePickerView:(UITextField *)textField;
- (void)setDatePickerView:(UITextField *)textField date:(NSDate *)date;
// 空の項目がないかチェック
- (BOOL)validateEmptyTextField:(NSArray *)textFields;

@end

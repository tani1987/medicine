//
//  RegisterInfo2ViewController.m
//  MLManager
//
//  Created by 黒田 真次郎 on 2015/06/23.
//  Copyright (c) 2015年 黒田 真次郎. All rights reserved.
//

#import "RegisterInfo2ViewController.h"

@interface RegisterInfo2ViewController ()<UIPickerViewDelegate, UIPickerViewDataSource>

@end

@implementation RegisterInfo2ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.nameTextField.delegate = self;
    self.medicalSpecialistTextField.delegate = self;
    self.workplaceTextField.delegate = self;
    self.classOfYearTextField.delegate = self;
    
    // 現在の年をを取得
    NSDate *today = [NSDate date];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear fromDate:today];
    NSInteger currentYear = components.year;
    self.classOfYearTextField.text = [NSString stringWithFormat:@"%d年度", currentYear];
    
    // ピッカー設定
    [self setPickerView:self.medicalSpecialistTextField items:[ItemDataSource medicalSpecialistArray]];
    [self setPickerView:self.workplaceTextField items:[ItemDataSource workplaceArray] obj:self.workplaceTextField.text];
    [self setPickerView:self.classOfYearTextField items:[ItemDataSource graduateYearArray] obj:self.classOfYearTextField.text];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Action Method

- (IBAction)didTapBackButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)didTapNextButton:(id)sender
{
    // 空の項目がないかチェック
    if (![self validateEmptyTextField:@[self.nameTextField]]) {
        return;
    }
    [self.navigationController pushViewController:self.info3Vc animated:YES];
}

@end

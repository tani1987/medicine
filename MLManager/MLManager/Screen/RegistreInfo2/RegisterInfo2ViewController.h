//
//  RegisterInfo2ViewController.h
//  MLManager
//
//  Created by 黒田 真次郎 on 2015/06/23.
//  Copyright (c) 2015年 黒田 真次郎. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "RegisterInfo3ViewController.h"

@interface RegisterInfo2ViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *medicalSpecialistTextField;
@property (weak, nonatomic) IBOutlet UITextField *workplaceTextField;
@property (weak, nonatomic) IBOutlet UITextField *classOfYearTextField;

@property (nonatomic) RegisterInfo3ViewController *info3Vc;

- (IBAction)didTapBackButton:(id)sender;
- (IBAction)didTapNextButton:(id)sender;

@end

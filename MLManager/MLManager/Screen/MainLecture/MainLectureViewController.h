//
//  MainLectureViewController.h
//  MLManager
//
//  Created by 黒田 真次郎 on 2015/06/23.
//  Copyright (c) 2015年 黒田 真次郎. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface MainLectureViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)didTapAddUnitButton:(id)sender;
- (IBAction)didTapEditProfileButton:(id)sender;

@end

//
//  MainLectureViewController.m
//  MLManager
//
//  Created by 黒田 真次郎 on 2015/06/23.
//  Copyright (c) 2015年 黒田 真次郎. All rights reserved.
//

#import "MainLectureViewController.h"
#import "MainLectureTableViewCell.h"
#import "AddLectureViewController.h"
#import "EditProfileViewController.h"

@interface MainLectureViewController ()<UITableViewDataSource, UITableViewDelegate>

@end

@implementation MainLectureViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    UINib *nib = [UINib nibWithNibName:@"MainLectureTableViewCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"Cell"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;
}

#pragma mark - UITableViewDelegate Method

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MainLectureTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//    // チェックマークをつける
//    cell.accessoryType = UITableViewCellAccessoryCheckmark;
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // 選択がはずれたセルを取得
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    // セルのアクセサリを解除する（チェックマークを外す）
    cell.accessoryType = UITableViewCellAccessoryNone;
}

#pragma mark - Action Method

- (IBAction)didTapAddUnitButton:(id)sender
{
    AddLectureViewController *addLectureVc = [[AddLectureViewController alloc] init];
    [self.navigationController pushViewController:addLectureVc animated:YES];
}

- (IBAction)didTapEditProfileButton:(id)sender
{
    EditProfileViewController *editProfVc = [[EditProfileViewController alloc] init];
    [self.navigationController pushViewController:editProfVc animated:YES];
}

@end

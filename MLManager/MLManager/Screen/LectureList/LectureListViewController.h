//
//  LectureListViewController.h
//  MLManager
//
//  Created by 黒田 真次郎 on 2015/06/23.
//  Copyright (c) 2015年 黒田 真次郎. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LectureListViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)didTapAddLectureButton:(id)sender;
- (IBAction)didTapDoneRegisterButton:(id)sender;

@end

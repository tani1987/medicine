//
//  LectureListViewController.m
//  MLManager
//
//  Created by 黒田 真次郎 on 2015/06/23.
//  Copyright (c) 2015年 黒田 真次郎. All rights reserved.
//

#import "LectureListViewController.h"
#import "LectureListCell.h"
#import "AddLectureViewController.h"
#import "MainLectureViewController.h"

@interface LectureListViewController ()<UITableViewDataSource, UITableViewDelegate>

@end

@implementation LectureListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // テーブルビュー設定
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    UINib *nib = [UINib nibWithNibName:@"LectureListCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"Cell"];
}

- (void)viewDidAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - UITableViewDelegate Method

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LectureListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    // チェックマークをつける
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // 選択がはずれたセルを取得
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    // セルのアクセサリを解除する（チェックマークを外す）
    cell.accessoryType = UITableViewCellAccessoryNone;
}

#pragma mark - Action Method

- (IBAction)didTapAddLectureButton:(id)sender
{
    AddLectureViewController *addLectureVc = [[AddLectureViewController alloc] init];
    [self.navigationController pushViewController:addLectureVc animated:YES];
}

- (IBAction)didTapDoneRegisterButton:(id)sender
{
    MainLectureViewController *mainLectureVc = [[MainLectureViewController alloc] init];
    UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:mainLectureVc];
    navCon.navigationBarHidden = YES;

    [self presentViewController:navCon
                       animated:YES completion:nil];
}

@end

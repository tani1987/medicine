//
//  LectureListCell.h
//  MLManager
//
//  Created by 黒田 真次郎 on 2015/06/25.
//  Copyright (c) 2015年 黒田 真次郎. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LectureListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *societyNameLabel;

@end
